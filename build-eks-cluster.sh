#!/usr/bin/env bash

echo "Injecting enviornment values..............."
echo "Using AWS Accoount id : ${AWS_ACCOUNT_ID}"
echo "Using AWS region      : ${AWS_DEFAULT_REGION}"
EKS_CLUSTER_NAME="poc"
EKS_ENVIRONMENT="dev"
IAC_TERRAFORM="infra/aws"
CLUSTER_NAME="${EKS_CLUSTER_NAME}-${EKS_ENVIRONMENT}"
EKS_K8S_CONFIG_MAP_AWS_AUTH="$(pwd)/config-${CLUSTER_NAME}-map-aws-auth.yaml"
EKS_K8S_CONFIG="$(pwd)/config-${CLUSTER_NAME}.yaml"
EKS_CLUSTER_VERSION="1.14"
echo "Preparing to build cluster named [$CLUSTER_NAME] with EKS version [$EKS_CLUSTER_VERSION}] "
EKS_WORKERS_AMI_NAMES="[\"amazon-eks-node-${EKS_CLUSTER_VERSION}*\",]"
EKS_AVAILABILITY_ZONES_COUNT=2
EKS_USERS_DEVELOPERS_TEAM=[""]
EKS_AWS_MAP_ACCOUNTS="${AWS_ACCOUNT_ID}"
EKS_USERS_WHITELIST_CIDRS="[\"51.37.76.4/32\",]"
EKS_NODE_AUTOSCALING_GENERAL="{\"desired_capacity\" = \"1\",\"min_size\" = \"1\", \"max_size\" = \"10\",\"instance_type\" = \"t3a.medium\" }"
TERRAFORM_STATE_S3_REGION="us-east-2"
TERRAFORM_STATE_S3_BUCKET="pchaudhuri-tf-state"
terraform --version
cd ${IAC_TERRAFORM} || exit 1
RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed to find the IaC code for infrastructure."
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Initializing terraform......"
terraform init -backend-config="key=${EKS_ENVIRONMENT}/${EKS_CLUSTER_NAME}.tfstate" -backend-config="region=${TERRAFORM_STATE_S3_REGION}" -backend-config="bucket=${TERRAFORM_STATE_S3_BUCKET}" .

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed initialing terraform"
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Validating terraform IaC templates....."
terraform validate .

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed validating terraform templates"
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Validating generating infrastructure plan ........"
terraform plan -input=false \
-var "aws-default-region=${AWS_DEFAULT_REGION}" \
-var "cluster-name=${CLUSTER_NAME}" \
-var "eks-cluster-version=${EKS_CLUSTER_VERSION}" \
-var "k8-availability-zones-count=${EKS_AVAILABILITY_ZONES_COUNT}" \
-var "eks-workers-ami-names=${EKS_WORKERS_AMI_NAMES}" \
-var "eks-node-autoscaling-general=${EKS_NODE_AUTOSCALING_GENERAL}" \
-var "users-developers-team=${EKS_USERS_DEVELOPERS_TEAM}" \
-var "eks-aws-map-accounts=${EKS_AWS_MAP_ACCOUNTS}" \
-var "eks-users-whitelist-cidrs=${EKS_USERS_WHITELIST_CIDRS}" .

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed creating terraform plan"
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Applying the terraform plan....."
terraform apply -auto-approve \
-var "aws-default-region=${AWS_DEFAULT_REGION}" \
-var "cluster-name=${CLUSTER_NAME}" \
-var "eks-cluster-version=${EKS_CLUSTER_VERSION}" \
-var "k8-availability-zones-count=${EKS_AVAILABILITY_ZONES_COUNT}" \
-var "eks-workers-ami-names=${EKS_WORKERS_AMI_NAMES}" \
-var "eks-node-autoscaling-general=${EKS_NODE_AUTOSCALING_GENERAL}" \
-var "users-developers-team=${EKS_USERS_DEVELOPERS_TEAM}" \
-var "eks-aws-map-accounts=${EKS_AWS_MAP_ACCOUNTS}" \
-var "eks-users-whitelist-cidrs=${EKS_USERS_WHITELIST_CIDRS}" .

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed applying terraform plan"
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "The applied plan recorded in S3 bucket '${TERRAFORM_STATE_S3_BUCKET}' with state '${EKS_ENVIRONMENT}/${EKS_CLUSTER_NAME}.tfstate' in the region '${AWS_DEFAULT_REGION}'"

# Assert EKS cluster is active.
# If active, then apply aws auth config, to enable nodes joining the EKS master
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Creating kubernetes configuration file '${EKS_K8S_CONFIG}'"
terraform output kubeconfig >"${EKS_K8S_CONFIG}"
echo "Checking for EKS based kubernetes cluster to be active"
kubectl --kubeconfig="${EKS_K8S_CONFIG}" cluster-info

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed applying terraform plan"
  exit $RESULT
else
  echo "###############################################################################################################"
  echo "CONGRATULATIONS. The server is ready."
  echo "Use config file at ${EKS_K8S_CONFIG} to access the cluster. "
  echo "###############################################################################################################"
fi

########################################################################################################################
# Kubernetes Hardening
# Various policies will be defined to harden the Kubernetes cluster
########################################################################################################################
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Creating the AWS authentication config map '${EKS_K8S_CONFIG_MAP_AWS_AUTH}', to enable Nodes joining kubernetes cluster"
terraform output config-map-aws-auth >"${EKS_K8S_CONFIG_MAP_AWS_AUTH}"
kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f "${EKS_K8S_CONFIG_MAP_AWS_AUTH}"

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Hardening the cluster..."
envsubst <../kubernetes/policies/default.yaml | kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f -
export EKS_GROUP_DEVELOPERS="$(terraform output group-developers)"
export EKS_NAMESPACE_GROUP_DEVELOPERS="arcper-$(terraform output group-developers)"
envsubst <../kubernetes/policies/role-developer.yaml | kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f -

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "PRINTING DETAILS OF THE CLUSTER."
echo "-----------------------------------"
echo "cluster_id                        : $(terraform output cluster_id)"
echo "cluster_certificate_authority_data: $(terraform output cluster_certificate_authority_data)"
echo "cluster_endpoint                  : $(terraform output cluster_endpoint)"
echo "cluster_version                   : $(terraform output cluster_version)"
echo "cluster_platform_version          : $(terraform output cluster_platform_version)"
echo "cluster_security_group_id         : $(terraform output cluster_security_group_id)"
echo "workers_asg_arns                  : $(terraform output workers_asg_arns)"
echo "worker_security_group_id          : $(terraform output worker_security_group_id)"
echo "worker_iam_role_name              : $(terraform output worker_iam_role_name)"
echo "worker_iam_role_arn               : $(terraform output worker_iam_role_arn)"
echo "group-developers                  : $(terraform output group-developers)"
echo "aws-autoscaler-role               : $(terraform output aws-autoscaler-role)"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

#######################################################################################################################
# Deployment of Kuberenetes Dashboard
#######################################################################################################################
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Installing the Dashboard..."
KUBERNETES_DASHBOARD_VERSION="2.0.0-rc2"
kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f "https://raw.githubusercontent.com/kubernetes/dashboard/v${KUBERNETES_DASHBOARD_VERSION}/aio/deploy/recommended.yaml"

#######################################################################################################################
# Deployment of Node autoscaler
#######################################################################################################################
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Installing the cluster autoscaler..."
export NAMESPACE=kube-system
export AWS_AUTOSCALER_ROLE="$(terraform output aws-autoscaler-role)"
# Should match with Kubernetes version
# Ref: https://console.cloud.google.com/gcr/images/google-containers/GLOBAL/
export CLUSTER_AUTOSCALER_VERSION="1.14.7"
envsubst <../kubernetes/cluster-autoscaler/cluster-autoscaler-ds.yaml | kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f -

########################################################################################################################
# KUBE2IAM
########################################################################################################################
#echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
#echo "Installing the Kube2Iam..."
#echo "Creating kubernetes configuration file '${EKS_K8S_CONFIG}'"
#export NAMESPACE=arcutl-kube2iam
#export KUBE2IAM_AWS_ACCESS_KEY_ID="$(terraform output kube2iam_access_key)"
#export KUBE2IAM_AWS_SECRET_ACCESS_KEY="$(terraform output kube2iam_secret_key)"
#export KUBE2IAM_AWS_DEFAULT_REGION="${AWS_DEFAULT_REGION}"
#export KUBE2IAM_IMAGE_TAG=latest
#envsubst '${NAMESPACE},${KUBE2IAM_BASE_ROLE_ARN},${KUBE2IAM_IMAGE_TAG},${KUBE2IAM_AWS_ACCESS_KEY_ID},${KUBE2IAM_AWS_SECRET_ACCESS_KEY},${KUBE2IAM_AWS_DEFAULT_REGION}' <../kubernetes/kube2iam/deploy.yaml | kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f -

########################################################################################################################
#  AWS ALB Ingress Controller
# Ref: https://kubernetes-sigs.github.io/aws-alb-ingress-controller/
########################################################################################################################
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Installing the AWS ALB ingress..."
export NAMESPACE=arcutl-alb-ingress
export AWS_ALB_INGRESS_CONTROLLER_VERSION="1.1.5"
envsubst <../kubernetes/alb-ingress-contoller/alb-ingress.yaml | kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f -


########################################################################################################################
#  AWS ALB Ingress Controller
# Ref: https://kubernetes-sigs.github.io/aws-alb-ingress-controller/
########################################################################################################################
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Installing the Network Pilicy manager Calico..."
kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/master/config/v1.5/aws-k8s-cni.yaml
kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/master/config/v1.5/cni-metrics-helper.yaml
kubectl --kubeconfig="${EKS_K8S_CONFIG}" apply -f https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/master/config/v1.5/calico.yaml
