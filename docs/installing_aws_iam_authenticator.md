# Installing `aws-iam-authenticator`

- Install the Go programming language for your operating system if you do not already have go installed. For more information, see [Install the Go tools](https://golang.org/doc/install#install) in the Go documentation
- Use `go get` to install the aws-iam-authenticator binary
```bash
go get -u -v github.com/kubernetes-sigs/aws-iam-authenticator/cmd/aws-iam-authenticator
```
