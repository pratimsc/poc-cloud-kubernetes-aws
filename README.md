# Kubernetes Cluster creation on AWS

## Manual using AWS Cloud formation
The [document](docs/README_CLOUDFORMATION.md) has instructions on creating a Kubernetes farm using AWS cloudformation and interactive commands

## Automated process using `terraform`
The [document](docs/README_TERRAFORM_AWS.md) has instructions on creating a Kubernetes farm using `terraform`, in a automated way.


> Please note these creations are **NOT** fit for production use, but for educational/PoC use.
