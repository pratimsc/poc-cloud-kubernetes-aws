#!/usr/bin/env bash

echo "Injecting enviornment values..............."
echo "Using AWS region ${AWS_DEFAULT_REGION}"
EKS_CLUSTER_NAME="poc"
EKS_ENVIRONMENT="dev"
IAC_TERRAFORM="infra/aws"
CLUSTER_NAME="${EKS_CLUSTER_NAME}-${EKS_ENVIRONMENT}"
EKS_CLUSTER_VERSION="1.14"
echo "Preparing to build cluster named [$CLUSTER_NAME] with EKS version [$EKS_CLUSTER_VERSION}] "
EKS_WORKERS_AMI_NAMES="[\"amazon-eks-node-${EKS_CLUSTER_VERSION}*\",]"
EKS_AVAILABILITY_ZONES_COUNT=1
EKS_USERS_DEVELOPERS_TEAM=[""]
EKS_AWS_MAP_ACCOUNTS="${AWS_ACCOUNT_ID}"
EKS_NODE_AUTOSCALING_GENERAL="{\"desired_capacity\" = \"0\",\"min_size\" = \"0\", \"max_size\" = \"10\",\"instance_type\" = \"t3a.medium\" }"
TERRAFORM_STATE_S3_REGION="us-east-2"
TERRAFORM_STATE_S3_BUCKET="pchaudhuri-tf-state"
terraform --version
cd ${IAC_TERRAFORM} || exit 1

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Initializing terraform......"
terraform init -backend-config="key=${EKS_ENVIRONMENT}/${EKS_CLUSTER_NAME}.tfstate" -backend-config="region=${TERRAFORM_STATE_S3_REGION}" -backend-config="bucket=${TERRAFORM_STATE_S3_BUCKET}" .

RESULT=$?
if [ $RESULT != 0 ]; then
  echo "Failed initialing terraform"
  exit $RESULT
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Destroying cluster ${CLUSTER_NAME}"
terraform destroy -auto-approve \
-var "aws-default-region=${AWS_DEFAULT_REGION}" \
-var "cluster-name=${CLUSTER_NAME}" \
-var "eks-cluster-version=${EKS_CLUSTER_VERSION}" \
-var "k8-availability-zones-count=${EKS_AVAILABILITY_ZONES_COUNT}" \
-var "eks-workers-ami-names=${EKS_WORKERS_AMI_NAMES}" \
-var "eks-node-autoscaling-general=${EKS_NODE_AUTOSCALING_GENERAL}" \
-var "users-developers-team=${EKS_USERS_DEVELOPERS_TEAM}" \
-var "eks-aws-map-accounts=${EKS_AWS_MAP_ACCOUNTS}" .
