#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "k8-vpc" {
  cidr_block = var.k8-vpc-cidr-block

  # VPC must have DNS hostname and DNS resolution support. Otherwise, your worker nodes cannot register with your cluster.
  # Ref: https://forums.aws.amazon.com/thread.jspa?messageID=868232
  enable_dns_hostnames = "true"
  enable_dns_support = "true"

  tags = {
    "Name" = "${var.cluster-name}-vpc"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}

# Public subnet for EKS Cluster
resource "aws_subnet" "k8-cluster" {
  count = var.k8-availability-zones-count

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = cidrsubnet(aws_vpc.k8-vpc.cidr_block, 8, count.index)
  vpc_id = aws_vpc.k8-vpc.id

  tags = {
    "Name" = "${var.cluster-name}-cluster"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
    "kubernetes.io/role/elb" = "1"
  }
}

# Private subnets for EKS Nodes
resource "aws_subnet" "k8-worker" {
  count = var.k8-availability-zones-count

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = cidrsubnet(  aws_vpc.k8-vpc.cidr_block, 8, count.index + length(aws_subnet.k8-cluster), )
  vpc_id = aws_vpc.k8-vpc.id
  map_public_ip_on_launch = "false"

  tags = {
    "Name" = "${var.cluster-name}-worker"
    "kubernetes.io/cluster/${var.cluster-name}" = "owned"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

# Create internet gateway for EKS Cluster
resource "aws_internet_gateway" "k8-cluster" {
  vpc_id = aws_vpc.k8-vpc.id

  tags = {
    Name = var.cluster-name
  }
}

# Create Elastic IP address for Nat Gateway
resource "aws_eip" "k8-worker-nat" {
  depends_on = [
    aws_internet_gateway.k8-cluster]
  vpc = true

  tags = {
    Name = "${var.cluster-name}-worker"
  }
}

# Create a NAT for outgoing internet traffic for Worker node
resource "aws_nat_gateway" "k8-worker" {
  allocation_id = aws_eip.k8-worker-nat.id
  subnet_id = aws_subnet.k8-cluster[0].id
  depends_on = [
    aws_internet_gateway.k8-cluster]
  tags = {
    Name = "${var.cluster-name}-worker"
  }
}

resource "aws_route_table" "k8-cluster" {
  vpc_id = aws_vpc.k8-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.k8-cluster.id
  }
  tags = {
    Name = "${var.cluster-name}-cluster"
  }
}

resource "aws_route_table_association" "k8-cluster" {
  count = var.k8-availability-zones-count

  route_table_id = aws_route_table.k8-cluster.id
  subnet_id = aws_subnet.k8-cluster[count.index].id
}

resource "aws_route_table" "k8-worker" {
  vpc_id = aws_vpc.k8-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.k8-worker.id
  }

  tags = {
    Name = "${var.cluster-name}-worker"
  }
}

resource "aws_route_table_association" "k8-worker" {
  count = var.k8-availability-zones-count

  route_table_id = aws_route_table.k8-worker.id
  subnet_id = aws_subnet.k8-worker[count.index].id
}

# Make the route table of the Worker nodes as default route table
resource "aws_main_route_table_association" "main" {
  route_table_id = aws_route_table.k8-worker.id
  vpc_id = aws_vpc.k8-vpc.id
}

