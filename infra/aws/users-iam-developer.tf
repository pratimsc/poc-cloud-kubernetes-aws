# IAM roles for allowing users to access EKS based K8s cluster
# Ref: https://ifritltd.com/2018/09/09/kubernetes-authentication-with-aws-iam/
# Ref: https://aws.amazon.com/blogs/opensource/integrating-ldap-ad-users-kubernetes-rbac-aws-iam-authenticator-project/

locals {
  developer-role-policy-doc = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY


  developers-group-policy-doc = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": [
                "${aws_iam_role.developer-role.arn}"
            ]
        }
    ]
}
POLICY


  developer-role-name = "${var.cluster-name}-dev-role"
}

# Developers
resource "aws_iam_role" "developer-role" {
  name = substr(local.developer-role-name, 0, min(64, length(local.developer-role-name)), )
  description = "Role for accessing kubernetes as developer."
  assume_role_policy = local.developer-role-policy-doc
  force_detach_policies = "true"
}

resource "aws_iam_policy" "developers-group-policy" {
  policy = local.developers-group-policy-doc
}

resource "aws_iam_group" "developers" {
  name = "${var.cluster-name}-dev"
}

resource "aws_iam_group_membership" "developers-team" {
  name = "${var.cluster-name}-dev-team"
  users = var.users-developers-team
  group = aws_iam_group.developers.name
}

resource "aws_iam_group_policy_attachment" "developers" {
  group = aws_iam_group.developers.name
  policy_arn = aws_iam_policy.developers-group-policy.arn
}

