#
# EKS Cluster Resources
#  * IAM Role to allow EKS service to manage other AWS services
#  * EC2 Security Group to allow networking traffic with EKS cluster
#  * EKS Cluster
#

resource "aws_iam_role" "k8-cluster" {
  name = "${var.cluster-name}-cluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY


  force_detach_policies = "true"
}

resource "aws_iam_role_policy_attachment" "k8-policy-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role = aws_iam_role.k8-cluster.name
}

resource "aws_iam_role_policy_attachment" "k8-policy-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role = aws_iam_role.k8-cluster.name
}

# Recommended security rules from -
# https://docs.aws.amazon.com/eks/latest/userguide/sec-group-reqs.html
resource "aws_security_group" "k8-cluster" {
  name = "${var.cluster-name}-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id = aws_vpc.k8-vpc.id
  tags = {
    "Name" = "${var.cluster-name}-cluster"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}

resource "aws_security_group_rule" "k8-cluster-ingress-node-https" {
  description = "Allow pods to communicate with the cluster API Server"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = aws_security_group.k8-cluster.id
  source_security_group_id = aws_security_group.eks-workers.id
  type = "ingress"
}

resource "aws_security_group_rule" "k8-cluster-egress-node" {
  description = "Allow the cluster API Server to communicate with pods"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = aws_security_group.k8-cluster.id
  source_security_group_id = aws_security_group.eks-workers.id
  type = "egress"
}

########################################################################################################################
# Allow access from Workstation creating the cluster
# Note: Should remain deactivated, when automation tools is used
# Hardened to provide only inbound access
########################################################################################################################
//resource "aws_security_group_rule" "k8-cluster-ingress-workstation-https" {
//  description = "Allow a workstation to communicate with the cluster API Server"
//  from_port = 443
//  to_port = 443
//  protocol = "tcp"
//  security_group_id = "${aws_security_group.k8-cluster.id}"
//  cidr_blocks = [
//    "${local.eks-users-full-whitelist}"]
//  type = "ingress"
//}

resource "aws_eks_cluster" "k8-cluster" {
  name = var.cluster-name
  role_arn = aws_iam_role.k8-cluster.arn
  version = var.eks-cluster-version

  vpc_config {
    endpoint_public_access = true
    endpoint_private_access = true
    security_group_ids = [
      aws_security_group.k8-cluster.id,
    ]
    subnet_ids = aws_subnet.k8-cluster.*.id
  }

  depends_on = [
    aws_iam_role_policy_attachment.k8-policy-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.k8-policy-AmazonEKSServicePolicy,
  ]
}

