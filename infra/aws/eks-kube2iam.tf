######################################################################################################
# Kube2iam
# Ref:
# - https://github.com/jtblin/kube2iam
# - https://www.bluematador.com/blog/iam-access-in-kubernetes-installing-kube2iam-in-production
# - https://octopus.com/blog/aws-roles
#
# QA Tip:
# execute below command to fetch the role.
# `curl http://169.254.169.254/latest/meta-data/iam/security-credentials/`
######################################################################################################

######################################################################################################
# Policy for kube2iam
######################################################################################################

resource "aws_iam_policy" "eks-kube2iam" {
  name_prefix = "${var.cluster-name}-kube2iam"
  policy      = data.aws_iam_policy_document.eks-workers-kube2iam.json
}

resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-kube2iam" {
  policy_arn = aws_iam_policy.eks-kube2iam.arn
  role       = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_role_policy_attachment" "eks-worker-utils-policy-kube2iam" {
  policy_arn = aws_iam_policy.eks-kube2iam.arn
  role       = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_user_policy_attachment" "kube2iam_assume_role_user" {
  policy_arn = aws_iam_policy.eks-kube2iam.arn
  user       = aws_iam_user.kube2iam.name
}

######################################################################################################
# Temporary IAM user kube2iam
######################################################################################################
resource "aws_iam_user" "kube2iam" {
  name = "svc-${local.name_prefix_general}-kube2iam"
  path = "/iac/"
  tags = {
    cluster = var.cluster-name
  }
}

resource "aws_iam_access_key" "kube2iam" {
  user = aws_iam_user.kube2iam.name
}
