resource "aws_security_group" "eks-workers" {
  name_prefix = "${var.cluster-name}-wrkr"
  description = "Security group for all worker nodes in the cluster ${var.cluster-name}"
  vpc_id      = aws_vpc.k8-vpc.id

  tags = {
    "Name"                                      = "${var.cluster-name}-worker"
    "kubernetes.io/cluster/${var.cluster-name}" = "owned"
  }
}

resource "aws_security_group_rule" "egress-all" {
  description = "Allow all egress traffic"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = [
    "0.0.0.0/0",
  ]
  security_group_id = aws_security_group.eks-workers.id
  type              = "egress"
}

resource "aws_security_group_rule" "eks-workers-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  security_group_id        = aws_security_group.eks-workers.id
  source_security_group_id = aws_security_group.eks-workers.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-workers-ingress-cluster-1" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks-workers.id
  source_security_group_id = aws_security_group.k8-cluster.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-workers-ingress-cluster-2" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks-workers.id
  source_security_group_id = aws_security_group.k8-cluster.id
  type                     = "ingress"
}

//resource "aws_security_group_rule" "ingress_security_groups" {
//  count                    = "${var.enabled == "true" ? length(var.allowed_security_groups) : 0}"
//  description              = "Allow inbound traffic from existing Security Groups"
//  from_port                = 0
//  to_port                  = 65535
//  protocol                 = "-1"
//  source_security_group_id = "${element(var.allowed_security_groups, count.index)}"
//  security_group_id        = "${join("", aws_security_group.default.*.id)}"
//  type                     = "ingress"
//}
//resource "aws_security_group_rule" "ingress_cidr_blocks" {
//  count             = "${var.enabled == "true" && length(var.allowed_cidr_blocks) > 0 ? 1 : 0}"
//  description       = "Allow inbound traffic from CIDR blocks"
//  from_port         = 0
//  to_port           = 0
//  protocol          = "-1"
//  cidr_blocks       = ["${var.allowed_cidr_blocks}"]
//  security_group_id = "${join("", aws_security_group.default.*.id)}"
//  type              = "ingress"
//}
