# Template for onboarding and off-boarding users on K8s cluster

locals {
  kubeconfig-developers = <<KUBECONFIG
apiVersion: v1
preferences: {}
kind: Config

clusters:
- cluster:
    server: ${aws_eks_cluster.k8-cluster.endpoint}
    certificate-authority-data: ${aws_eks_cluster.k8-cluster.certificate_authority[0].data}
  name: ${aws_eks_cluster.k8-cluster.name}
contexts:
- context:
    cluster: ${aws_eks_cluster.k8-cluster.name}
    user: aws
    namespace: ${aws_iam_group.developers.name}
  name: aws
current-context: aws
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${aws_eks_cluster.k8-cluster.name}"
      env:
        - name: AWS_PROFILE
          value: <<AWS profile name in aws configuration file>>
KUBECONFIG

}

