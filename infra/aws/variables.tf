#
# Variables Configuration
#
variable "aws-default-region" {
  default = "eu-central-1"
  type = string
}

variable "cluster-name" {
  default = "architect-poc-k8"
  type = string
}

variable "eks-cluster-version" {
  description = "EKS cluster version"
  type = string
}

variable "k8-vpc-cidr-block" {
  default = "192.168.0.0/16"
  type = string
}

variable "eks-node-autoscaling-general" {
  default = {
    "desired_capacity" = "0"
    "min_size" = "0"
    "max_size" = "10"
    "instance_type" = "t3a.medium"
  }
  type = map(string)
}

variable "eks-node-general-block-device" {
  description = "Properties of block device for EC2 instances of General worker nodes"
  default = {
    "delete_on_termination" = "true"
    "volume_type" = "gp2"
    "volume_size" = 50
  }
  type = map(string)
}

variable "eks-node-utils-block-device" {
  description = "Properties of block device for EC2 instances of Utility worker nodes"
  default = {
    "delete_on_termination" = "true"
    "volume_type" = "gp2"
    "volume_size" = 50
  }
  type = map(string)
}

variable "k8-availability-zones-count" {
  type = string
  default = "1"
}

variable "eks-workers-ami-names" {
  type = list(string)
  default = [
    "*",
  ]
}

variable "eks-workers-ami-owners" {
  type = list(string)

  # Amazon owned AMIs
  default = [
    "amazon",
  ]
}

variable "terraform-state-s3-bucket" {
  type = string

  # This is used as placeholder to force evaluation of this variable.
  # However, its used by helper script, during `terraform init` process, and not post init.
  default = "terraform-state-eks"
}

variable "eks-users-whitelist-cidrs" {
  type = list(string)
  description = "List of CIDRs that are allowed to access the EKS as admin or developers"
  default = [
    ""]
}

variable "users-developers-team" {
  type = list(string)
  description = "List of developers requiring access to EKS"
}


variable "eks-aws-map-accounts" {
  type = string
  description = "AWS accounts whose IAM users are allowed to authenticate with EKS"
}
