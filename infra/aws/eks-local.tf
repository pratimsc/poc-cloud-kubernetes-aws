# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/amazon-eks-nodegroup.yaml
# Ref : https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2018-08-30/amazon-eks-nodegroup.yaml
# Ref : https://github.com/awslabs/amazon-eks-ami/blob/master/files/bootstrap.sh
locals {
  eks-worker-general-userdata = <<USERDATA
#!/bin/bash -xe
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.k8-cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.k8-cluster.certificate_authority[0].data}' --kubelet-extra-args '--node-labels=general=true,general-compute=true,role=general,role=general-compute' "${var.cluster-name}"

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> I AM DONE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
systemctl status kubelet
USERDATA

temp_name_prefix_general = "${var.cluster-name}-general"

name_prefix_general = substr(local.temp_name_prefix_general,0,min(32, length(local.temp_name_prefix_general)),)
name_prefix_autoscaler = "${var.cluster-name}-autoscaler"
}

