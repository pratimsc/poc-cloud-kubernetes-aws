#
# Workstation External IP
#
# This configuration is not required and is
# only provided as an example to easily fetch
# the external IP of your local workstation to
# configure inbound EC2 Security Group access
# to the Kubernetes cluster.
# This will only allow access from site/machine
# which has been used to initiate the Kubernetes farm
#

data "http" "workstation-external-ip" {
  url = "http://ipv4.icanhazip.com"
}

# Override with variable or hardcoded value if necessary
locals {
  workstation-external-cidr = [
    "${chomp(data.http.workstation-external-ip.body)}/32"]
  eks-users-full-whitelist = concat(local.workstation-external-cidr,var.eks-users-whitelist-cidrs,)
}

######################################################################################################
# Policy for troubleshooting workstation
######################################################################################################
data "aws_ami" "workstation" {
  filter {
  name = "name"
  values = ["amzn-ami-hvm-*"]
  }
  most_recent = true
  owners = ["amazon"]
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// NOT TO BE IMPLEMENTED IN PRODUCTION
// Temporary add any SSH key for debugging.
/////////////////////////////////////////////////////////////////////////////////////////////////////
//resource "aws_security_group_rule" "ssh" {
//  from_port = 22
//  protocol = "tcp"
//  cidr_blocks = ["<<Bastion workstation id>>"]
//  security_group_id = "${aws_security_group.k8-workers.id}"
//  to_port = 22
//  type = "ingress"
//}
//resource "aws_security_group_rule" "bastion_ssh" {
//  from_port = 22
//  to_port = 22
//  protocol = "tcp"
//  security_group_id = "${aws_security_group.eks-workers.id}"
//  source_security_group_id = "${aws_security_group.eks-troubleshooting.id}"
//  type = "ingress"
//}
//resource "aws_security_group" "eks-troubleshooting" {
//  name_prefix = "${var.cluster_name}-troubleshooting"
//  description = "Security group for all worker nodes in the cluster ${var.cluster_name}"
//  vpc_id = "${aws_vpc.k8-vpc.id}"
//
//  tags = "${
//    map(
//     "Name", "${var.cluster_name}-worker",
//     "kubernetes.io/cluster/${var.cluster_name}", "owned",
//    )
//  }"
//}
//
//resource "aws_security_group_rule" "workstation-troubleshooter" {
//  from_port = 22
//  protocol = "tcp"
//  security_group_id = "${aws_security_group.eks-troubleshooting.id}"
//  cidr_blocks = [
//    "0.0.0.0/0"]
//  to_port = 22
//  type = "ingress"
//}
//
//resource "aws_security_group_rule" "workstation-troubleshooter-egress" {
//  from_port = 0
//  protocol = "tcp"
//  security_group_id = "${aws_security_group.eks-troubleshooting.id}"
//  cidr_blocks = ["0.0.0.0/0"]
//  to_port = 65535
//  type = "egress"
//}
//
//resource "aws_instance" "workstation" {
//  tags {
//    Name = "${var.cluster_name}-workstation"
//  }
//  ami = "${data.aws_ami.eks-workers-ami.id}"
//  instance_type = "t2.medium"
//  security_groups = [
//    "${aws_security_group.eks-troubleshooting.id}"]
//  key_name = "troubleshootingkey"
//  associate_public_ip_address = "true"
//  subnet_id = "${aws_subnet.k8-cluster.*.id[0]}"
//}
