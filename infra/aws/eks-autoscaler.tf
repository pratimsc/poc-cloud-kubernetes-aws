######################################################################################################
# IAM role for Autoscaler
######################################################################################################
resource "aws_iam_role" "eks-autoscaler" {
  name = local.name_prefix_autoscaler

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY


  force_detach_policies = "true"
  path = "/"
}

######################################################################################################
# Policy for autoscaler
######################################################################################################
resource "aws_iam_policy" "eks-autoscaler" {
  name_prefix = local.name_prefix_autoscaler
  policy = data.aws_iam_policy_document.eks-autoscaler.json
}

resource "aws_iam_role_policy_attachment" "eks-autoscaler" {
  policy_arn = aws_iam_policy.eks-autoscaler.arn
  role = aws_iam_role.eks-autoscaler.name
}

//resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEKSWorkerNodePolicy" {
//  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
//  role = "${aws_iam_role.eks-autoscaler.name}"
//}
//
//resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEKS_CNI_Policy" {
//  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
//  role = "${aws_iam_role.eks-autoscaler.name}"
//}
//
//resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEC2ContainerRegistryReadOnly" {
//  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
//  role = "${aws_iam_role.eks-autoscaler.name}"
//}
//
//resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-autoscaler" {
//  policy_arn = "${aws_iam_policy.eks-workers-general-autoscaler.arn}"
//  role = "${aws_iam_role.eks-autoscaler.name}"
//}
//
