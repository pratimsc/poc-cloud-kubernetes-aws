#!/usr/bin/env bash

function prop {
    grep "${1}" $VAL_FILE | cut -d'=' -f2 | cut -d'"' -f2
}


###############################################################################
# Building Kubernetes Cluster
###############################################################################
# Terraform variable file
VAL_FILE=$1
# Terraform directory
TERRAFORM_TEMPLATE_DIR=$2

if [ ! -f ${VAL_FILE} ]; then
    echo "ERROR: You must provide a VALUE file that will be used to pass values to Terraform template."
    echo "ERROR: e.g. `basename "$0"` /path/to/cluster-values.tfvars"
    exit 01
fi

if [ ! -d ${TERRAFORM_TEMPLATE_DIR} ]; then
    TERRAFORM_TEMPLATE_DIR="."
fi

echo "INFO: Initializing the terraform environment"
echo "INFO: Reading values from file ${VAL_FILE}"

BACKEND_BUCKET=$(prop "terraform-state-s3-bucket")
if [ -z "${BACKEND_BUCKET}" ]; then
    echo "ERROR: A value with key 'terraform-state-s3-bucket' must be provided in ${VAL_FILE} that points to S3 bucket storing the state"
    exit 02
fi

BACKEND_EKS_ENVIRONMENT=$(prop "eks-environment")
if [ -z "${BACKEND_EKS_ENVIRONMENT}" ]; then
    echo "ERROR: A value with key 'eks-environment' must be provided in ${VAL_FILE} that mentions the environment"
    exit 03
fi

CLUSTER_NAME=$(prop "cluster-name" )
if [ -z "${CLUSTER_NAME}" ]; then
    echo "ERROR: A value with key 'cluster-name' must be provided in ${VAL_FILE}"
    exit 04
fi

BACKEND_REGION=$(prop "aws-default-region")
if [ -z "${BACKEND_REGION}" ]; then
    echo "ERROR: A value with key 'aws-default-region' must be provided in ${VAL_FILE} that mentions the AWS region for S3 bucket"
    exit 05
fi

BACKEND_KEY="${BACKEND_EKS_ENVIRONMENT}/${CLUSTER_NAME}.tfstate"
echo "terraform init \
    -backend-config='key=${BACKEND_KEY}' \
    -backend-config='region=${BACKEND_REGION}' \
    -backend-config='bucket=${BACKEND_BUCKET}' \
    ${TERRAFORM_TEMPLATE_DIR}"

terraform init \
    -backend-config="key=${BACKEND_KEY}" \
    -backend-config="region=${BACKEND_REGION}" \
    -backend-config="bucket=${BACKEND_BUCKET}" \
    ${TERRAFORM_TEMPLATE_DIR}

echo "INFO: Creating terraform plan in S3 bucket '${BACKEND_BUCKET}' with key '${BACKEND_KEY}' in the region '${BACKEND_REGION}'"
terraform plan -var-file=${VAL_FILE} ${TERRAFORM_TEMPLATE_DIR}


echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
echo "INFO: Applying the plan and storing the state in S3 bucket '${BACKEND_BUCKET}' with key '${BACKEND_KEY}' in the region '${BACKEND_REGION}'"
terraform apply -var-file=${VAL_FILE} -auto-approve

###############################################################################
# Display the Terraform state after successful 'apply'
###############################################################################

echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
echo "INFO: Displaying the applied plan recorded in S3 bucket '${BACKEND_BUCKET}' with key '${BACKEND_KEY}' in the region '${BACKEND_REGION}'"
terraform show 


###############################################################################
# Configuring Kubernetes Cluster
###############################################################################
HOME=`echo $HOME`
KUBE_HOME="${HOME}/.kube"
echo "INFO: Creating kubernetes configuration directory '${KUBE_HOME}', if its absent"
if [ ! -d "${KUBE_HOME}" ]; then
    echo "INFO: Creating '${KUBE_HOME}' "
    mkdir "$KUBE_HOME"
fi
KUBECONFIG=$(echo "${KUBE_HOME}/config-${CLUSTER_NAME}.yaml")
echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
echo "INFO: Adding kubernetes configuration file '${KUBECONFIG}' at '${KUBE_HOME}'"
# terraform output -state=${STATE_OUT} kubeconfig > ${KUBECONFIG}
terraform output kubeconfig > ${KUBECONFIG}

KUBE_CONFIG_MAP_AWS_AUTH=$(echo "${KUBE_HOME}/config-${CLUSTER_NAME}-config-map-aws-auth.yaml")
echo "INFO: Applying the AWS authentication config map ${KUBE_CONFIG_MAP_AWS_AUTH}, to enable Nodes joining kubernetes cluster"
# terraform output -state=${STATE_OUT} config-map-aws-auth > ${KUBE_CONFIG_MAP_AWS_AUTH}
terraform output config-map-aws-auth > ${KUBE_CONFIG_MAP_AWS_AUTH}
kubectl --kubeconfig=${KUBECONFIG} apply -f ${KUBE_CONFIG_MAP_AWS_AUTH}

echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
echo "INFO: Kubernetes cluster ${CLUSTER_NAME} has been successfully CREATED !"
echo "INFO: Details of the cluser is - "
kubectl --kubeconfig=${KUBECONFIG} cluster-info
echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
echo "INFO: The terraform state is present at ${STATE_OUT}. Please keep it safe, as it is needed for tearing the cluster down."
# echo "INFO: The cluster ${CLUSTER_NAME} can be destroyed by executing 'terraform destroy -state=${STATE_OUT} ${TERRAFORM_TEMPLATE_DIR}"
echo "INFO: The cluster ${CLUSTER_NAME} can be destroyed by executing 'terraform destroy ${TERRAFORM_TEMPLATE_DIR}'"
echo "------------------------------------------------------------------------"
echo "------------------------------------------------------------------------"
