#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EC2 Security Group to allow networking traffic
#  * Data source to fetch latest EKS worker AMI
#  * AutoScaling Launch Configuration to configure worker instances
#  * AutoScaling Group to launch worker instances
#
######################################################################################################
# IAM role for General compute nodes
######################################################################################################
resource "aws_iam_role" "eks-worker-general" {
  name = local.name_prefix_general

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY


  force_detach_policies = "true"
  path = "/"
}

######################################################################################################
# Policy for autoscaler
######################################################################################################
resource "aws_iam_policy" "eks-workers-general-autoscaler" {
  name_prefix = local.name_prefix_general
  policy = data.aws_iam_policy_document.eks-autoscaler.json
}

resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_role_policy_attachment" "eks-worker-general-policy-autoscaler" {
  policy_arn = aws_iam_policy.eks-workers-general-autoscaler.arn
  role = aws_iam_role.eks-worker-general.name
}

resource "aws_iam_instance_profile" "eks-worker-general" {
  name_prefix = local.name_prefix_general
  role = aws_iam_role.eks-worker-general.name
}

# Launch immutable instances that can not be logged into
resource "aws_launch_configuration" "k8-workers" {
  name_prefix = local.name_prefix_general
  associate_public_ip_address = false
  iam_instance_profile = aws_iam_instance_profile.eks-worker-general.name
  image_id = data.aws_ami.eks-workers-ami.id
  instance_type = var.eks-node-autoscaling-general["instance_type"]
  root_block_device {
    delete_on_termination = var.eks-node-general-block-device["delete_on_termination"]
    volume_type = var.eks-node-general-block-device["volume_type"]
    volume_size = var.eks-node-general-block-device["volume_size"]
  }
  security_groups = [
    aws_security_group.eks-workers.id,
  ]
  user_data_base64 = base64encode(local.eks-worker-general-userdata)

  lifecycle {
    create_before_destroy = true
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // NOT TO BE IMPLEMENTED IN PRODUCTION
  // Temporary add any SSH key for debugging.
  // key_name = "eks_worker_node_ssh"
  /////////////////////////////////////////////////////////////////////////////////////////////////////
}

resource "aws_autoscaling_group" "k8-workers" {
  count = var.k8-availability-zones-count

  name_prefix = local.name_prefix_general
  launch_configuration = aws_launch_configuration.k8-workers.id

  // Remove the desired capacity, as it forces scaled down when CI/CD pipeline kicks in.
  // The kubernetes autoscaler will scale down when the cluster does not need any EC2 instances.
  // The `min_size` will ensure that at least 1 EC2 instance is available.
  // desired_capacity = "${var.eks-node-autoscaling-general["desired_capacity"]}"
  min_size = var.eks-node-autoscaling-general["min_size"]
  max_size = var.eks-node-autoscaling-general["max_size"]
  vpc_zone_identifier = [
    aws_subnet.k8-worker[count.index].id,
  ]

  tag {
    key = "Name"
    value = "${var.cluster-name}-general"
    propagate_at_launch = true
  }

  tag {
    key = "kubernetes.io/cluster/${var.cluster-name}"
    value = "owned"
    propagate_at_launch = true
  }

  tag {
    key = "k8s.io/cluster-autoscaler/enabled"
    propagate_at_launch = true
    value = "true"
  }

  tag {
    key = "k8s.io/cluster-autoscaler/${var.cluster-name}"
    propagate_at_launch = true
    value = "true"
  }

  tag {
    key = "general-compute"
    propagate_at_launch = true
    value = "true"
  }
}

