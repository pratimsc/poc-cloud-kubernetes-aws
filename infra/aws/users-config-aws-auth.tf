# Template for onboarding and off-boarding users on K8s cluster

locals {
  config-map-aws-auth = <<CONFIGMAPAWSAUTH
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.eks-worker-general.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: ${aws_iam_role.developer-role.arn}
      username: grp-developers
      groups:
        - ${aws_iam_group.developers.name}

  mapAccounts: |
    - "${var.eks-aws-map-accounts}"
CONFIGMAPAWSAUTH

}
