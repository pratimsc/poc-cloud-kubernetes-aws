######################################################################################################
# Current AWS user ID
######################################################################################################

data "aws_caller_identity" "current" {
}

######################################################################################################
# Nodes for General compute
######################################################################################################

data "aws_ami" "eks-workers-ami" {
  filter {
    name   = "name"
    values = var.eks-workers-ami-names
  }
  most_recent = true
  owners      = var.eks-workers-ami-owners
}

######################################################################################################
# Policy for autoscaler
######################################################################################################
data "aws_iam_policy_document" "eks-autoscaler" {
  statement {
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:DescribeTags",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
    ]
    resources = [
      "*",
    ]
  }
}

######################################################################################################
# Policy for autoscaler
######################################################################################################
data "aws_iam_policy_document" "eks-workers-kube2iam" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    resources = [
      "*",
    ]
  }
}

