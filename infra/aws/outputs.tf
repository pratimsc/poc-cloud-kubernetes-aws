#
# Outputs
#

locals {
  kubeconfig = <<KUBECONFIG

apiVersion: v1
preferences: {}
kind: Config

clusters:
- cluster:
    server: ${aws_eks_cluster.k8-cluster.endpoint}
    certificate-authority-data: ${aws_eks_cluster.k8-cluster.certificate_authority[0].data}
  name: ${aws_eks_cluster.k8-cluster.name}
contexts:
- context:
    cluster: ${aws_eks_cluster.k8-cluster.name}
    user: aws
  name: aws
current-context: aws
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${aws_eks_cluster.k8-cluster.name}"
KUBECONFIG

}

output "config-map-aws-auth" {
  value = local.config-map-aws-auth
}

output "kubeconfig" {
  value = local.kubeconfig
}

output "kubeconfig-developers" {
  value = local.kubeconfig-developers
}

output "group-developers" {
  description = "Group belonging to developers"
  value = aws_iam_group.developers.name
}

output "cluster_id" {
  description = "The name/id of the EKS cluster."
  value = aws_eks_cluster.k8-cluster.id
}

output "cluster_certificate_authority_data" {
  description = "Nested attribute containing certificate-authority-data for cluster. This is the base64 encoded certificate data required to communicate with your cluster."
  value = aws_eks_cluster.k8-cluster.certificate_authority[0].data
}

output "cluster_endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value = aws_eks_cluster.k8-cluster.endpoint
}

output "cluster_version" {
  description = "The Kubernetes server version for the EKS cluster."
  value = aws_eks_cluster.k8-cluster.version
}

output "cluster_platform_version" {
  description = "Platform version of the EKS cluster"
  value = aws_eks_cluster.k8-cluster.platform_version
}

output "cluster_security_group_id" {
  description = "Security group ID attached to the EKS cluster."
  value = aws_security_group.k8-cluster.id
}

output "workers_asg_arns" {
  description = "IDs of the autoscaling groups containing workers."
  value = aws_autoscaling_group.k8-workers.*.arn
}

output "worker_security_group_id" {
  description = "Security group ID attached to the EKS workers."
  value = aws_security_group.eks-workers.id
}

output "worker_iam_role_name" {
  description = "IAM role name attached to EKS workers"
  value = aws_iam_role.eks-worker-general.name
}

output "worker_iam_role_arn" {
  description = "IAM role ID attached to EKS workers"
  value = aws_iam_role.eks-worker-general.arn
}

output "kube2iam_access_key" {
  description = "IAM user used for kube2iam"
  value = aws_iam_access_key.kube2iam.id
}

output "kube2iam_secret_key" {
  description = "IAM secret used for kube2iam"
  value = aws_iam_access_key.kube2iam.secret
}

output "aws-autoscaler-role" {
  description = "IAM role to be used by the cluster auto scaler"
  value = aws_iam_role.eks-autoscaler.name
}

