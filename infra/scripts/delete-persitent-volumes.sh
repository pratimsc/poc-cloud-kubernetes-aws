#!/usr/bin/env bash

export PVS=$(kubectl get persistentvolumes| grep pvc | cut -f1 -d" " | xargs)
if [ "${PVS}" == "" ]; then
  echo -e "No persistent volumes to delete."
else
  echo -e "Deleting persistent volumes => [${PVS}]"
  kubectl delete persistentvolumes ${PVS}
fi
echo -e "Housekeeping operation complete."
