#!/usr/bin/env bash

export NAMESPACES=$(kubectl get namespace -o name | awk '!/default/ && !/kube-public/ && !/kube-system/' | xargs)

if [ "${NAMESPACES}" == "" ]; then
  echo -e "No namespaces to delete."
else
  echo -e "Deleting namespaces => [${NAMESPACES}]"
  kubectl delete ${NAMESPACES}
fi
echo -e "Environment cleaning operation complete."
